package com.example.trafficimagesapp.domain.usecase.data

data class TrafficMap (
    val lat: Double,
    val longitude: Double,
    val imgUrl: String,
    val timestamp: String
)