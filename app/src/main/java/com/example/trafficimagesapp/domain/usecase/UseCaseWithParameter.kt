package com.example.trafficimagesapp.domain.usecase

public interface UseCaseWithOutParameter<R> {
    public abstract fun execute(): R
}