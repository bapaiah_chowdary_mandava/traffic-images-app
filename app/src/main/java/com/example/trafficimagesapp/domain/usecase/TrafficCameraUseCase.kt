package com.example.trafficimagesapp.domain.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.trafficimagesapp.data.model.TrafficImageResponse
import com.example.trafficimagesapp.repository.TrafficCameraRepository
import com.example.trafficimagesapp.util.TrafficCameraCoroutineDispatcher

import io.reactivex.functions.Cancellable
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

internal class TrafficCameraUseCase(
    private val trafficCameraRepository : TrafficCameraRepository,
    private val trafficCameraCoroutineDispatcher: TrafficCameraCoroutineDispatcher
): UseCaseWithOutParameter<LiveData<Response<TrafficImageResponse>>>,
          Cancellable,CoroutineScope  {

    private var job : Job? = null
    override val coroutineContext: CoroutineContext
        get() = trafficCameraCoroutineDispatcher.io

    override fun execute(): LiveData<Response<TrafficImageResponse>> {
       val result = MutableLiveData<Response<TrafficImageResponse>>()
        job = launch {
           val response = trafficCameraRepository.requestTrafficData()
            withContext(trafficCameraCoroutineDispatcher.main) {
                result.postValue(response)
            }
        }
        return result
    }

    override fun cancel() {
        if(coroutineContext.isActive) job?.cancel()
    }
}