package com.example.trafficimagesapp

object Constants {
  const val TRAFFIC_IMAGES ="transport/traffic-images"
  const val BASE_URL ="https://api.data.gov.sg/v1/"
}