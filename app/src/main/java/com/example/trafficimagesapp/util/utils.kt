package com.example.trafficimagesapp.util

import java.text.SimpleDateFormat

object utils {

    fun dateFormat(date:String?):String {
        if(!date.isNullOrEmpty()) {
            val dateValue = SimpleDateFormat("yyyy-MM-dd").parse(date)
            val newF = SimpleDateFormat("MMM dd, YYYY")
            return newF.format(dateValue)
        }
        else{
            return ""
        }
    }
}

enum class ServiceType{
    API,
    MOCK
}