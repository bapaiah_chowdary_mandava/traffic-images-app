package com.example.trafficimagesapp.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

internal open class TrafficCameraCoroutineDispatcher {
    open val main: CoroutineDispatcher by lazy { Dispatchers.Main}
    open val io: CoroutineDispatcher by lazy {Dispatchers.IO}
}