package com.example.trafficimagesapp


import android.content.Context
import com.example.trafficimagesapp.domain.usecase.TrafficCameraUseCase
import com.example.trafficimagesapp.network.Api
import com.example.trafficimagesapp.network.ServiceInterface
import com.example.trafficimagesapp.repository.TrafficCameraRepository
import com.example.trafficimagesapp.ui.TrafficSharedViewModel
import com.example.trafficimagesapp.util.TrafficCameraCoroutineDispatcher
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * App Module to load all the Koin injections
 *
 */
val appModule= module {
    factory { getTrafficDataService() }
    viewModel { TrafficSharedViewModel(get()) }
    factory { TrafficCameraRepository(get()) }
    single { TrafficCameraUseCase(get(),get()) }
    single { TrafficCameraCoroutineDispatcher() }



}

fun getTrafficDataService(): ServiceInterface? = Api().getServiceinterface()
