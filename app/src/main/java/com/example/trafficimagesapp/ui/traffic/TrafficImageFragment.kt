package com.example.trafficimagesapp.ui.traffic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation.findNavController
import com.example.trafficimagesapp.MainActivity
import com.example.trafficimagesapp.R
import com.example.trafficimagesapp.domain.usecase.data.TrafficMap
import com.example.trafficimagesapp.ui.TrafficSharedViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_traffic_image.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TrafficImageFragment : Fragment() , OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {
    val TAG = TrafficImageFragment::class.java.canonicalName
    private var mMap: GoogleMap? = null
    private val sharedViewModel by sharedViewModel<TrafficSharedViewModel>()
    private lateinit var mActivity : MainActivity


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callTrafficData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_traffic_image, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mActivity = activity as MainActivity
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        mapView.onResume()
        setUpLoader()
        setUpTrafficSucessHandling()
        setUpTrafficErrorHandling()
        setupTrafficMapData()

    }

    private fun setUpLoader() {

    }
    private fun setUpTrafficSucessHandling() {
      sharedViewModel.onTrafficDataSuccessEvent.observe(viewLifecycleOwner, Observer {
        it.let { trafficImageReponse ->
            sharedViewModel.setMarkersInfoForTrafficData(trafficImageReponse)
        }
      })
    }


    private fun setUpTrafficErrorHandling() {
        sharedViewModel.showTrafficDataFailureEvent.observe(viewLifecycleOwner, Observer {
            it.let { trafficImageResults ->

            }
        })
    }

    private fun setupTrafficMapData(){
        sharedViewModel.trafficData.observe(viewLifecycleOwner, Observer{
            setPointersOnMap(it)
        })
    }

    fun callTrafficData(){
        sharedViewModel.trafficDataTrigger.postValue(Unit)
    }

    //map marker click
    override fun onMarkerClick(marker: Marker?): Boolean {
        navigateAndShowImage()
        return false
    }



    private fun setPointersOnMap(trafficMapList: List<TrafficMap>) {
        trafficMapList.forEach {
            val markerOptions =  MarkerOptions().position(LatLng(it.lat, it.longitude)).visible(true).anchor(0.5f, 0.5f)
            val marker = mMap?.addMarker(markerOptions)
            marker?.tag = it
            marker?.showInfoWindow()
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.lat, it.longitude),12f))
            mMap?.setOnMarkerClickListener(this)
        }
    }

    fun navigateAndShowImage() {
        findNavController(rootMapLayout).navigate(R.id.action_trafficImageFragment_to_ImageFragment)
    }

    //Display error snackbar and onclicking the retry call the API again
    private fun displayError(msg:String){
       /* AlertUtils.showSnackBar(binding.root,msg,"retry",object :View.OnClickListener{
            override fun onClick(v: View?) {
                initObservers()
            }
        })*/
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
    }

}