package com.example.trafficimagesapp.ui

import android.util.Log
import androidx.lifecycle.*
import com.example.trafficimagesapp.data.model.TrafficImageResponse
import com.example.trafficimagesapp.domain.usecase.TrafficCameraUseCase
import com.example.trafficimagesapp.domain.usecase.data.TrafficMap
import retrofit2.Response
import java.lang.Exception

internal class TrafficSharedViewModel(
    private val trafficCameraUseCase: TrafficCameraUseCase
): ViewModel() {


    internal val trafficDataTrigger = MutableLiveData<Unit>()
    private val _trafficData : MutableLiveData<List<TrafficMap>> = MutableLiveData()
    val trafficData : LiveData<List<TrafficMap>>
            get() = _trafficData


    private val trafficDataResult : LiveData<Response<TrafficImageResponse>> =
        Transformations.switchMap(trafficDataTrigger) {
            it?.let {
                trafficCameraUseCase.execute()
            }
        }

    private val onTrafficDataSuccessTrigger = MediatorLiveData<TrafficImageResponse>()
    val onTrafficDataSuccessEvent : LiveData<TrafficImageResponse> get() = onTrafficDataSuccessTrigger

    private val showTrafficDataFailureTrigger = MediatorLiveData<String>()
    val showTrafficDataFailureEvent : LiveData<String> get() = showTrafficDataFailureTrigger


    init {
        onTrafficDataSuccessTrigger.addSource(trafficDataResult) {
            if(it.isSuccessful) onTrafficDataSuccessTrigger.postValue(it.body())
        }

        showTrafficDataFailureTrigger.addSource(trafficDataResult) {
            if(!it.isSuccessful) showTrafficDataFailureTrigger.postValue(it.message())
        }
    }


    fun setMarkersInfoForTrafficData(it: TrafficImageResponse) {
        var trafficMapList = listOf<TrafficMap>()
        it?.items?.forEach {
            it.cameras.forEach {
                Log.i("tag","traffic item::"+it.location.latitude)
                val trafficMapItem = TrafficMap(
                    it.location.latitude,
                    it.location.longitude,
                    it.image,
                    it.timestamp
                )
                trafficMapList = trafficMapList.plus(trafficMapItem)
            }
        }
        _trafficData.value = trafficMapList
    }


}