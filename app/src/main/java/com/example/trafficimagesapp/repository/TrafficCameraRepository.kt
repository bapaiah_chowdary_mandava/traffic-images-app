package com.example.trafficimagesapp.repository

import com.example.trafficimagesapp.data.model.TrafficImageResponse
import com.example.trafficimagesapp.network.ServiceInterface
import retrofit2.Response


class TrafficCameraRepository(private val retrofitService: ServiceInterface) {

    suspend fun requestTrafficData(): Response<TrafficImageResponse> {
      return retrofitService.getTrafficImages()
    }
}