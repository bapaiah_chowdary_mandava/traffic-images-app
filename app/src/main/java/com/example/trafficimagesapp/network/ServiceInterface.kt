package com.example.trafficimagesapp.network


import com.example.trafficimagesapp.Constants.TRAFFIC_IMAGES
import com.example.trafficimagesapp.data.model.TrafficImageResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

/**
 * Service Interface to fetch data
 *
 */
interface ServiceInterface {
    @GET(TRAFFIC_IMAGES)
    suspend fun getTrafficImages() : Response<TrafficImageResponse>
}