package com.example.trafficimagesapp.network

import android.content.Context
import com.example.trafficimagesapp.Constants
import com.example.trafficimagesapp.Constants.BASE_URL
import com.example.trafficimagesapp.util.ServiceType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

class Api() {
    private var serviceInterface: ServiceInterface?=null
    init {
        serviceInterface =
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build().create(ServiceInterface::class.java)
    }

    fun getServiceinterface(): ServiceInterface? {
        return serviceInterface
    }
}